<!DOCTYPE html> <html lang="ru"> <head>
  <link rel="stylesheet"
			href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<script
			src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="style.css">
    <title> Задание 1 </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width= device-width, initial-scale=1.0">
  </head>
  <body>
    <div style="height: 70px; text-align: center; background: #BC8F9D; color: #fff; width: 100%">
    <div class="header container">
      <div id="picture"><h1 style="margin: 0 auto;"> <img src="https://www.pinclipart.com/picdir/big/203-2031252_peanuts-box-clip-art-peanuts-free-engine-image.png" style="margin-right: 5px;" width = "45" alt="Кусь"></h1></div>
      <div><h1 id="site"> Unknown site name </h1></div>
    </div>
    </div>
    <div class="px-0 px-sm-3 container">
    <nav id="menu" class="container px-0 mb-3">
       <div class="col-12 my-0 my-md-2 mx-1 py-0 py-md-2 col-md"><a href="#links"> Ссылки </a></div>
       <div class="col-12 my-0 my-md-2 mx-1 py-0 py-md-2 col-md"><a href="#form"> Форма </a> </div>
       <div class="col-12 my-0 my-md-2 mx-1 py-0 py-md-2 col-md"><a href="#table2"> Таблица </a> </div>
    </nav>
</div>
  <div class="form container col-12 mt-3 mb-0">
    <h1 class="linkformtable" style="padding: 7px;" >Заполните форму для нас: </h1>
    <form class="pl-3" action="."
    method="POST">

    <label>
      Имя:<br />
      <input name="name"
        value="Ваше имя" />
    </label><br />

    <label>
      Email:<br />
      <input name="email"
        value="Ваш email"
        type="email" />
    </label><br />

    <label>
      Дата рождения:<br />
      <input name="birthday"
        value="2019-08-13"
        type="date" />
    </label><br />
    <label>
      Биография:<br />
      <textarea name="biography">В день, когда я родился...</textarea>
    </label><br />

      Количество ваших конечностей: <br />
      <label>
        <input type="radio" checked="checked"
        name="limbs" value="1" />
        От 0 до 5
      </label>
      <label>
        <input type="radio"
        name="limbs" value="2" />
        От 6 до 10
      </label>
      <label>
        <input type="radio"
          name="limbs" value="3" />
          11 и более
        </label>
        <br />

      Укажите ваши сверхспособности:
      <br />
      <select name="abilitiess[]"
        multiple="multiple">
        <option value="1" selected="selected">Управление временем</option>
        <option value="2">Паучье чутье</option>
        <option value="3">Регенерация</option>
        <option value="4">Владение магией</option>
      </select>
    <br />

    Ваш пол:<br />
    <label><input type="radio" checked="checked"
      name="gender" value="M" />
      Муж</label>
    <label><input type="radio"
      name="gender" value="F" />
      Жен</label><br />

    <br />
    <p> <label><input type="checkbox" checked="checked"
      name="contract" />
      С контрактом ознакомлен</label></p>

    <input type="submit" value="Отправить" />
  </form>
</div>
    </div>
    </div>
<footer class="pt-3 pl-lg-5">
  (c) Тройнина Влада 2020
</footer>
  </body>
</html>

