<?php
header('Content-Type: text/html; charset=UTF-8');

error_reporting(E_ERROR | E_PARSE);

if ($_SERVER['REQUEST_METHOD'] == 'GET') { // Когда сервер получает get запрос, то  вставляется код из form.php и загружается страница
  include('form.php');
  if (!empty($_GET['save'])) { // Когда get запрос це save, то в код вставляется то, что в принте и выполняется
    print "<div id='success_form_response'>
                Спасибо, результаты сохранены.
            </div>";
  }
    exit();
}

$errors = FALSE;

$errors_message = "Отправка данных прервана из-за следующих ошибок: <br/>";

$trimmedPost = [];

foreach ($_POST as $key => $value)//записал значения всех полей
	if (is_string($value))
		$trimmedPost[$key] = trim($value);
	else
		$trimmedPost[$key] = $value;

if (empty($trimmedPost['name'])) {
  $errors_message.= 'Вы не заполнили имя.<br/>';
  $errors = TRUE;
}
if (!preg_match('/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/', $trimmedPost['email'])) {
  $errors_message.= 'Вы не заполнили email или ввели неверные данные.<br/>';
  $errors = TRUE;
}
if (!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $trimmedPost['birthday'])) {
  $errors_message.= 'Вы не заполнили дату рождения или ввели неверные данные.<br/>';
  $errors = TRUE;
}
if (!preg_match('/^[MF]$/', $trimmedPost['gender'])) {
  $errors_message.= 'Вы неверно заполнили пол.<br/>';
  $errors = TRUE;
}
if (!preg_match('/^[1-3]$/', $trimmedPost['limbs'])) {
  $errors_message.= 'Вы ввели недопустимое количество конечностей.<br/>';
  $errors = TRUE;
}
$superpowers_error = FALSE;
foreach ($trimmedPost['abilities'] as $v)
  if (!preg_match('/[1-3]/', $v)) {
    $superpowers_error = TRUE;
    $errors = TRUE;
  }
if($superpowers_error)
  $errors_message.= 'Вы ввели неверные суперспособности.<br/>';
if (!isset($trimmedPost['contract'])) {
  $errors_message.= 'Вы не ознакомились с контрактом.<br/>';
  $errors = TRUE;
}

if ($errors) {
  include('form.php');
  print "<link href='show_mb.css' rel='stylesheet'>
            <div id='modal_blackout'>
              <div id='success_form_response'>";
  echo "{$errors_message}";  
  print "</div>
            </div>";           
  exit();
}

$user = 'u41011';
$pass = '9363823';
$db = new PDO('mysql:host=localhost;dbname=u41011', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

try {
  $db->beginTransaction();
  $stmt1 = $db->prepare("INSERT INTO form SET name = ?, email = ?, birthday = ?, 
    gender = ? , limbs = ?, biography = ?");
  $stmt1 -> execute([$trimmedPost['name'], $trimmedPost['email'], $trimmedPost['birthday'], 
    $trimmedPost['gender'], $trimmedPost['limbs'], $trimmedPost['biography']]);
  $stmt2 = $db->prepare("INSERT INTO client_abilities SET id_client = ?, id_ability = ?");
  $id = $db->lastInsertId();
  foreach ($trimmedPost['abilitiess'] as $s)
    $stmt2 -> execute([$id, $s]);
  $db->commit();
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  $db->rollBack();
  exit();
}

header('Location: ?save=1');
